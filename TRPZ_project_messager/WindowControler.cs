﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TRPZ_project_messager
{
    static class WindowControler
    {
        public static void SetPosition(Window window,Page page)
        {
            window.Left = Window.GetWindow(page).Left;
            window.Top = Window.GetWindow(page).Top;
        }
        public static void SetPosition(Window first_window, Window second_window)
        {
            second_window.Left = first_window.Left;
            second_window.Top = first_window.Top;
        }
    }
}
