﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;
using TRPZ_project_messager.Windows;

namespace TRPZ_project_messager
{
    /// <summary>
    /// Логика взаимодействия для Sign_in.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
            
        }
        private void SignInClick(object sender, RoutedEventArgs e)
        {
            LoginModel login = new LoginModel();

            login.Login = _login.Text;
            login.Password = _login.Text;
            login.Id = Account.users[0].UserId;

            Account.myUser = Account.users[login.Id];

            if (_login.Text.Equals("admin"))
            {
                AdminWindow adminWindow = new AdminWindow();
                adminWindow.Show();
                this.Close();
            }
            else
            {
                AccountWindow accountWindow = new AccountWindow(Account.myUser);
            
                accountWindow.Show();
                this.Close();
            }
        }
        private void RegistrationClick(object sender, RoutedEventArgs e)
        {
            RegistrationWindow registrationWindow = new RegistrationWindow();
            registrationWindow.Show();
            this.Close();
        }
        private void ForgotPassClick(object sender, RoutedEventArgs e)
        {
            EmailVerificationWindow emailVerificationWindow = new EmailVerificationWindow();
            emailVerificationWindow.Show();
            this.Close();
        }
    }
}
