﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRPZ_project_messager.Model;

namespace TRPZ_project_messager
{
    class Account
    {
       
        static List<Devices> devices = new List<Devices>()
        {
            new Devices()
            {
                DeviceId = 0,
                DeviceIp = "1.1.1.1",
                DeviceName = "computer",
                DeviceTime = DateTime.Now
            },
            new Devices()
            {
                DeviceId = 1,
                DeviceIp = "0.0.0.0",
                DeviceName = "iphone",
                DeviceTime = DateTime.Now
            }

        };

        static public User myUser = new User();

        static public List<User> users = new List<User>()
        {
            new User()
            {
                UserId = 0,
                UserEmail = "dima@ukr.net",
                UserImg = "http://zabavnik.club/wp-content/uploads/Milye_kartinki_dlya_ld_1_06122128.jpg",
                UserLogin = "trofim0904",
                UserPassword = "1111",
                UserPhoneNumber = "+380000000000",
                UserRole = null,
                UserStatus = "online",
                UserListDevice = devices
            },
            new User()
            {
                UserId = 1,
                UserEmail = "anton@ukr.net",
                UserImg = "https://www.meme-arsenal.com/memes/be50e6ba99654b5455027dcc82beb5b3.jpg",
                UserLogin = "anton777",
                UserPassword = "2222",
                UserPhoneNumber = "+381111111111",
                UserRole = null,
                UserStatus = DateTime.Now.ToLongTimeString(),
            },
            new User()
            {
                UserId = 2,
                UserEmail = "sany@ukr.net",
                UserImg = "https://i.pinimg.com/originals/a4/44/fb/a444fbfac4facb912ad2a2cb9cf0f465.jpg",
                UserLogin = "sanu880",
                UserPassword = "3333",
                UserPhoneNumber = "+382222222222",
                UserRole = null,
                UserStatus = "online",
            },
            new User()
            {
                UserId = 3,
                UserEmail = "noone@ukr.net",
                UserImg = "https://cs7.pikabu.ru/post_img/2018/11/24/7/1543058804127851366.jpg",
                UserLogin = "noone",
                UserPassword = "4444",
                UserPhoneNumber = "+383333333333",
                UserRole = null,
                UserStatus = "online",
            }
        };

       
        static public List<UsersInChat> usersInChats = new List<UsersInChat>()
        {
            new UsersInChat()
            {
                UserInChatId = 0,
                UserId = 0,
                ChatId = 1
            },
            new UsersInChat()
            {
                UserInChatId = 1,
                UserId = 1,
                ChatId = 0
            },
            new UsersInChat()
            {
                UserInChatId = 2,
                UserId = 0,
                ChatId = 1
            },
            new UsersInChat()
            {
                UserInChatId = 3,
                UserId = 1,
                ChatId = 1
            },
            new UsersInChat()
            {
                UserInChatId = 4,
                UserId = 2,
                ChatId = 1
            },
            new UsersInChat()
            {
                UserInChatId = 5,
                UserId = 0,
                ChatId = 2
            },
            new UsersInChat()
            {
                UserInChatId = 6,
                UserId = 3,
                ChatId = 2
            }
        };

        static public List<Chats> chats = new List<Chats>()
        {
            new Chats()
            {
                ChatId = 0,
                ChatAdmin = null,
                ChatType = 1,
                ChatImg = "https://www.meme-arsenal.com/memes/be50e6ba99654b5455027dcc82beb5b3.jpg",
                ChatName = "anton777",
                ChatStatus="online"
            },
            new Chats()
            {
                ChatId = 1,
                ChatAdmin = users[2],
                ChatType = 2,
                ChatImg = "http://crosti.ru/patterns/00/01/03/31ea3ae4d8/picture.jpg",
                ChatName = "Our group",
                ChatStatus="3 users"
            },
            new Chats()
            {
                ChatId = 2,
                ChatAdmin = null,
                ChatType = 1,
                ChatImg = "https://cs7.pikabu.ru/post_img/2018/11/24/7/1543058804127851366.jpg",
                ChatName = "noone",
                ChatStatus = DateTime.Now.ToLongTimeString()

                
            }
        };
     
        static public List<Message> messages = new List<Message>()
        {
            new Message()
            {
                MessageId = 0,
                MessageText = "Et, donec magna. Odio pretium vivamus gravida dictumst dolor curabitur.",
                MessageTime = DateTime.Now,
                MessageUser = users[0]
            },
            new Message()
            {
                MessageId = 1,
                MessageText = "Magna hymenaeos conubia curabitur Sodales velit nonummy luctus phasellus elementum.",
                MessageTime = DateTime.Now,
                MessageUser = users[1]
            },
            new Message()
            {
                MessageId = 2,
                MessageText = "Class quis lobortis venenatis condimentum facilisis magnis scelerisque maecenas sagittis.",
                MessageTime = DateTime.Now,
                MessageUser = users[2]
            },
            new Message()
            {
                MessageId = 3,
                MessageText = "Rhoncus libero mollis pellentesque. Maecenas ornare vel interdum. Nascetur donec.",
                MessageTime = DateTime.Now,
                MessageUser = users[3]
            }
        };
        static public List<MessageInChat> messageInChats = new List<MessageInChat>()
        {
            new MessageInChat()
            {
                ChatId = 0,
                MessageId = 0
            },
            new MessageInChat()
            {
                ChatId = 1,
                MessageId = 1
            },
            new MessageInChat()
            {
                ChatId = 1,
                MessageId = 2
            },
            new MessageInChat()
            {
                ChatId = 2,
                MessageId = 3
            }
        };


        public static List<User> GetUsersFromChats(User user)
        {
            List<User> users = new List<User>();
            users.Add(Account.users[1]);
            users.Add(Account.users[3]);
            
            return users;
        }

        public static List<Chats> GetGroupChats(User user)
        {
            List<Chats> chats = new List<Chats>();

            chats.Add(Account.chats[1]);

            return chats;
        }

        public static List<Chats> GetSoloChats(User user)
        {
            List<Chats> chats = new List<Chats>();

            chats.Add(Account.chats[0]);
            chats.Add(Account.chats[2]);
            

            return chats;
        }
    }
}
