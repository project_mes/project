﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;
using TRPZ_project_messager.UC;

namespace TRPZ_project_messager.Pages
{
    /// <summary>
    /// Логика взаимодействия для IpConfigPage.xaml
    /// </summary>
    public partial class IpConfigPage : Page
    {
        public IpConfigPage(User user)
        {
            InitializeComponent();
            Devices devices = new Devices();
            foreach(Devices ipConfig in user.UserListDevice)
            {
                _deviceIp.Children.Add(new DeviceUC(ipConfig.DeviceId + 1, ipConfig.DeviceName, ipConfig.DeviceIp, ipConfig.DeviceTime.ToLongTimeString()));
            }
          
        }
    }
}
