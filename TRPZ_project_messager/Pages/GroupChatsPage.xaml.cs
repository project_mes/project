﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;
using TRPZ_project_messager.UC;
using TRPZ_project_messager.Windows;

namespace TRPZ_project_messager.Pages
{
    /// <summary>
    /// Логика взаимодействия для GroupChatsPage.xaml
    /// </summary>
    public partial class GroupChatsPage : Page
    {
        public GroupChatsPage(User user)
        {
            InitializeComponent();
            foreach(Chats chats in Account.GetGroupChats(user))
            {
                _groupChats.Children.Add(new UserViewInListUC(chats.ChatId, chats.ChatImg, chats.ChatName, chats.ChatStatus, 600));
            }
            //for (int i = 0; i < 20; i++)
            //    _groupChats.Children.Add(new UserViewInListUC("https://pngimage.net/wp-content/uploads/2018/06/rfhnbyrf-png-.png", "groupchat", "5 users",600));
        }
        private void AddNewChatClick(object sender, RoutedEventArgs e)
        {

            
            AllGroupsChatsWindow allGroupsChatsWindow = (AllGroupsChatsWindow)Window.GetWindow(this);


            allGroupsChatsWindow._workZoneGroupChats.Content = new AddNewGroupChatPage(Account.GetUsersFromChats(Account.myUser));
            _addNewChat.IsEnabled = false;

        }
    }
}
