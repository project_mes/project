﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;
using TRPZ_project_messager.UC;

namespace TRPZ_project_messager.Pages
{
    /// <summary>
    /// Логика взаимодействия для SoloChatsPage.xaml
    /// </summary>
    public partial class SoloChatsPage : Page
    {
        public SoloChatsPage(User user)
        {
            InitializeComponent();

            foreach(Chats chats in Account.GetSoloChats(user))
            {
                _soloChats.Children.Add(new UserViewInListUC(chats.ChatId ,chats.ChatImg, chats.ChatName, chats.ChatStatus, 600));
            }
            //for (int i = 0; i < 20; i++)
            //    _soloChats.Children.Add(new UserViewInListUC("https://i.pinimg.com/originals/87/af/ef/87afef76100d0b704ca5b6039468a736.jpg", "user", DateTime.Now.ToLongTimeString(), 600));
        }
    }
}
