﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.UC;
using TRPZ_project_messager.Windows;
using TRPZ_project_messager.Model;

namespace TRPZ_project_messager.Pages
{
    /// <summary>
    /// Логика взаимодействия для MessagesPage.xaml
    /// </summary>
    public partial class MessagesPage : Page
    {
        //Chats chat = new Chats();
        //int chatId;
        public MessagesPage()
        {
            InitializeComponent();
            for (int i = 0; i < 10; i++)
            {
                _messagesSp.Children.Add(new MessageUC());
            }
        }
        
        
        public MessagesPage(int id)
        {
            InitializeComponent();
            //chatId = id;
            foreach (MessageInChat message in Account.messageInChats)
            {

                if (message.ChatId == id)
                {
                    _titleTb.Text = Account.chats[id].ChatName;
                    _statusTb.Text = Account.chats[id].ChatStatus;
                    _messagesSp.Children.Add(new MessageUC(Account.messages[message.MessageId]));
                }

            }

        }

        //public MessagesPage(Chats chat)
        //{
        //    InitializeComponent();

        //    foreach(MessageInChat message in Account.messageInChats)
        //    {

        //        if (message.ChatId == chat.ChatId)
        //        {
        //            _titleTb.Text = chat.ChatName;
        //            _statusTb.Text = chat.ChatStatus;
        //            _messagesSp.Children.Add(new MessageUC(Account.messages[message.MessageId]));
        //        }

        //    }

        //}
        public MessagesPage(string title,string status)
        {
            InitializeComponent();
            _titleTb.Text = title;
            _statusTb.Text = status;
            for (int i = 0; i < 10; i++)
            {
                _messagesSp.Children.Add(new MessageUC());
            }
        }
        private void ChatInfoClick(object sender, RoutedEventArgs e)
        {
            if (Window.GetWindow(this).Title.Equals("AllGroupsChatsWindow"))
            {
                GroupChatInfoWindow groupChatInfoWindow = new GroupChatInfoWindow();
                groupChatInfoWindow.Owner = Window.GetWindow(this);
                groupChatInfoWindow.Show();
            }
            else
            {
                UserInfoWindow userInfo = new UserInfoWindow();
                userInfo.Owner = Window.GetWindow(this);
                userInfo.Show();
            }

        
        }

        private void BackClick(object sender, RoutedEventArgs e)
        {
            if (Window.GetWindow(this).Title.Equals("AllGroupsChatsWindow"))
            {
                AllGroupsChatsWindow allGroupsChatsWindow = (AllGroupsChatsWindow)Window.GetWindow(this);
                allGroupsChatsWindow._workZoneGroupChats.Content = new GroupChatsPage(Account.myUser);
            }
            else
            {
                AllChatsWindow allChatsWindow = (AllChatsWindow)Window.GetWindow(this);
                allChatsWindow._workZoneChats.Content = new SoloChatsPage(Account.myUser);
            }
        }
        private void SendMessageClick(object sender, RoutedEventArgs e)
        {
            Message message = new Message();
            message.MessageText = _inputMessageTb.Text;
            message.MessageTime = DateTime.Now;
            

        }
    }
}
