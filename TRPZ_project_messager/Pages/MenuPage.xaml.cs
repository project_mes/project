﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Windows;

namespace TRPZ_project_messager.Pages
{
    /// <summary>
    /// Логика взаимодействия для MenuPage.xaml
    /// </summary>
    public partial class MenuPage : Page
    {
        
        public MenuPage()
        {
            InitializeComponent();
        }

        private void MenuAboutClick(object sender, RoutedEventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            
            WindowControler.SetPosition(aboutWindow, this);
            aboutWindow.Show();
            Window.GetWindow(this).Close();
        }

        private void MenuExitClick(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            WindowControler.SetPosition(mainWindow, this);
            Window.GetWindow(this).Close();
        }

        private void MenuSearchClick(object sender, RoutedEventArgs e)
        {
            SearchWindow searchWindow = new SearchWindow();
            searchWindow.Show();
            WindowControler.SetPosition(searchWindow, this);
            Window.GetWindow(this).Close();
        }

        private void MenuIpClick(object sender, RoutedEventArgs e)
        {
            AllIpWindow allIpWindow = new AllIpWindow();
            allIpWindow.Show();
            WindowControler.SetPosition(allIpWindow, this);
            Window.GetWindow(this).Close();

        }

        private void MenuGroupChatsClick(object sender, RoutedEventArgs e)
        {
            AllGroupsChatsWindow allGroupsChatsWindow = new AllGroupsChatsWindow();
            allGroupsChatsWindow.Show();
            WindowControler.SetPosition(allGroupsChatsWindow, this);
            Window.GetWindow(this).Close();
        }

        private void MenuChatsClick(object sender, RoutedEventArgs e)
        {
            AllChatsWindow allChatsWindow = new AllChatsWindow();
            allChatsWindow.Show();
            WindowControler.SetPosition(allChatsWindow, this);
            Window.GetWindow(this).Close();
        }

        private void MenuAccountClick(object sender, RoutedEventArgs e)
        {
            AccountWindow accountWindow = new AccountWindow(Account.myUser);
            accountWindow.Show();
            WindowControler.SetPosition(accountWindow, this);
            Window.GetWindow(this).Close();
        }
    }
}
