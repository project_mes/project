﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;
using TRPZ_project_messager.UC;
using TRPZ_project_messager.Windows;

namespace TRPZ_project_messager.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddNewGroupChatPage.xaml
    /// </summary>
    public partial class AddNewGroupChatPage : Page
    {
       
        public AddNewGroupChatPage(List<User> users)
        {
            InitializeComponent();
            foreach(User user in users)
            {
                //if (chat.ChatId == user.ChatId)
                //{
                _scrolViewerUsers.Children.Add(new UserViewInListUC(user.UserId, user.UserImg, user.UserLogin, user.UserStatus, false));
                //}

            }
            //for (int i = 0; i < 20; i++)
            //{
            //    _scrolViewerUsers.Children.Add(new UserViewInListUC("https://www.meme-arsenal.com/memes/0e4da9683135de120cb39d9e5c683523.jpg", "user", DateTime.Now.ToLongTimeString(), false));
            //}
           
   
            
        }
        private void BackClick(object sender, RoutedEventArgs e)
        {
            
            AllGroupsChatsWindow allGroupsChatsWindow = (AllGroupsChatsWindow)Window.GetWindow(this);
            allGroupsChatsWindow._addNewChat.IsEnabled = true;
            allGroupsChatsWindow._workZoneGroupChats.Content = new GroupChatsPage(Account.myUser);
 
        }
    }
}
