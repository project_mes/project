﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;

namespace TRPZ_project_messager.UC
{
    /// <summary>
    /// Логика взаимодействия для messageUC.xaml
    /// </summary>
    public partial class MessageUC : UserControl
    {
        public MessageUC()
        {
            InitializeComponent();

        }
        public MessageUC(Message message)
        {
            InitializeComponent();
            _userNameTb.Text = message.MessageUser.UserLogin;
            _statusTb.Text = message.MessageTime.ToLongTimeString();
            _messageTb.Text = message.MessageText;
            BitmapImage image = new BitmapImage(new Uri(message.MessageUser.UserImg, UriKind.Absolute));
            _userImg.Source = image;
            

        }
    }
}
