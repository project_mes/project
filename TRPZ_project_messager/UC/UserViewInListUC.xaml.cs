﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Pages;
using TRPZ_project_messager.Windows;

namespace TRPZ_project_messager.UC
{
    /// <summary>
    /// Логика взаимодействия для UserViewInListUC.xaml
    /// </summary>
    public partial class UserViewInListUC : UserControl
    {
        int userControlId;

        public UserViewInListUC(int id, string img, string name, string status,bool active)
        {
           
            InitializeComponent();
            BitmapImage image = new BitmapImage(new Uri(img, UriKind.Absolute));
            if (!active)
            {
                _btn.Click -= OpenClick;
            }
            _imgMain.Source = image;
            _nameTb.Text = name;

            _statusTb.Text = status;
            userControlId = id;

        }
        public UserViewInListUC(int id, string img, string name, string status,int elementWidth)
        {

            InitializeComponent();
            _btn.Width = elementWidth;
            BitmapImage image = new BitmapImage(new Uri(img, UriKind.Absolute));
            _imgMain.Source = image;
            _nameTb.Text = name;

            _statusTb.Text = status;
            userControlId = id;
        }

        private void OpenClick(object sender, RoutedEventArgs e)
        {
            if(Window.GetWindow(this).Title.Equals("AllChatsWindow"))
            {
                if (Account.chats[userControlId].ChatType == 1)
                {
                    AllChatsWindow allChatsWindow = (AllChatsWindow)Window.GetWindow(this);
               
                    allChatsWindow._workZoneChats.Content = new MessagesPage(userControlId);
                }
            }
            if (Window.GetWindow(this).Title.Equals("AllGroupsChatsWindow"))
            {
                AllGroupsChatsWindow allGroupsChatsWindow = (AllGroupsChatsWindow)Window.GetWindow(this);
                allGroupsChatsWindow._workZoneGroupChats.Content = new MessagesPage(userControlId);
            }
            if(Window.GetWindow(this).Title.Equals("SearchWindow"))
            {
                UserInfoWindow userInfo = new UserInfoWindow(userControlId, "Search");
                userInfo.Owner = Window.GetWindow(this);
                userInfo.Show();
            }
            if (Window.GetWindow(this).Title.Equals("AdminWindow"))
            {
                UserInfoWindow userInfo = new UserInfoWindow();
                userInfo.Owner = Window.GetWindow(this);
                userInfo.Show();
            }
            if (Window.GetWindow(this).Title.Equals("GroupChatInfoWindow"))
            {
                UserInfoWindow userInfo = new UserInfoWindow();
                userInfo.Owner = Window.GetWindow(this);
                userInfo.Show();
            }
        }
    }
}
