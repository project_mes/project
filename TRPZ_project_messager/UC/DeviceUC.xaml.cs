﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TRPZ_project_messager.UC
{
    /// <summary>
    /// Логика взаимодействия для DeviceUC.xaml
    /// </summary>
    public partial class DeviceUC : UserControl
    {
        public DeviceUC()
        {
            InitializeComponent();
        }
        public DeviceUC(int id, string name, string ip, string time)
        {
            InitializeComponent();
            _idTb.Text = id.ToString();
            _nameTb.Text = name;
            _ipTb.Text = ip;
            _timeTb.Text = time;
        }
    }
}
