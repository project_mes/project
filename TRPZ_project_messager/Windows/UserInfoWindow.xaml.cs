﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для UserInfoWindow.xaml
    /// </summary>
    public partial class UserInfoWindow : Window
    {
        public UserInfoWindow()
        {
            InitializeComponent();
        }
        public UserInfoWindow(int idUser, string place)
        {
            InitializeComponent();
            _userEmail.Text = Account.users[idUser].UserEmail;
            _userLogin.Text = Account.users[idUser].UserLogin;
            _userNumber.Text = Account.users[idUser].UserPhoneNumber;
        }
        private void BackClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void AddToGroupChat(object sender, RoutedEventArgs e)
        {
            AddToGroupChatWindow addToGroup = new AddToGroupChatWindow();
            addToGroup.Owner = this;
            addToGroup.Show();
        }
        private void SendMessage(object sender, RoutedEventArgs e)
        {
            SendOneMessageWindow sendOneMessage = new SendOneMessageWindow();
            sendOneMessage.Owner = this;
            sendOneMessage.Show();

        }
        private void CheckIp(object sender, RoutedEventArgs e)
        {
            IpAdminCheckWindow ipAdminCheckWindow = new IpAdminCheckWindow();
            ipAdminCheckWindow.Owner = this;
            ipAdminCheckWindow.Show();
        }
    }
}
