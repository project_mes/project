﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для SendOneMessageWindow.xaml
    /// </summary>
    public partial class SendOneMessageWindow : Window
    {
        public SendOneMessageWindow()
        {
            InitializeComponent();
        }

        private void BackClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
