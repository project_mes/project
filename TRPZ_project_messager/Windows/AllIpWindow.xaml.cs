﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;
using TRPZ_project_messager.Pages;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для AllIpWindow.xaml
    /// </summary>
    public partial class AllIpWindow : Window
    {
        
      
        public AllIpWindow()
        {
            
            InitializeComponent();
            _ipConfig.Content = new IpConfigPage(Account.myUser);
            _menu.Content = new MenuPage();
           
           
        }
    }
}
