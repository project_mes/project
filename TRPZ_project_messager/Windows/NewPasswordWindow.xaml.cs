﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для NewPasswordWindow.xaml
    /// </summary>
    public partial class NewPasswordWindow : Window
    {
        public NewPasswordWindow()
        {
            InitializeComponent();
        }
        private void NewPassToSignClick(object sender, RoutedEventArgs e)
        {
            NewPasswordModel newPassword = new NewPasswordModel();

            newPassword.Password = _newPassword.Password.ToString();
            newPassword.RepeatPassword = _repeatPassword.Password.ToString();


            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}
