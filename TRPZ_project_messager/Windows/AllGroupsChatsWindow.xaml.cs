﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.Pages;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для AllGroupsChatsWindow.xaml
    /// </summary>
    public partial class AllGroupsChatsWindow : Window
    {
        public AllGroupsChatsWindow()
        {
            InitializeComponent();
            _menu.Content = new MenuPage();
            _workZoneGroupChats.Content = new GroupChatsPage(Account.myUser);
        }
        //Add_new_chat_click
        private void AddNewChatClick(object sender, RoutedEventArgs e)
        {

            //AddNewGroupChat addNewGroupChat = new AddNewGroupChat();
            //addNewGroupChat.Owner = this;
            //addNewGroupChat.Show();
            _workZoneGroupChats.Content = new AddNewGroupChatPage(Account.GetUsersFromChats(Account.myUser));
            _addNewChat.IsEnabled = false;

        }
    }
}
