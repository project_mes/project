﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для EmailVerificationWindow.xaml
    /// </summary>
    public partial class EmailVerificationWindow : Window
    {
        public EmailVerificationWindow()
        {
            InitializeComponent();
        }
        private void VerEmailClick(object sender, RoutedEventArgs e)
        {
            EmailVerificationModel emailVerification = new EmailVerificationModel();
            emailVerification.Code = _code.Text;

            NewPasswordWindow newPasswordWindow = new NewPasswordWindow();
            newPasswordWindow.Show();
            this.Close();
        }
    }
}
