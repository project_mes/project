﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.UC;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для GroupChatInfoWindow.xaml
    /// </summary>
    public partial class GroupChatInfoWindow : Window
    {
        public GroupChatInfoWindow()
        {
            InitializeComponent();
            for (int i = 0; i < 12; i++)
            {
                _usersList.Children.Add(new UserViewInListUC(1, "https://www.publicdomainpictures.net/pictures/300000/nahled/face-painted-on-rock-1555470638UJv.jpg", "user1", "online", true));
            }
        }

        private void BackClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
