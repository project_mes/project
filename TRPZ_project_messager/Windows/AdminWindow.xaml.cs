﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.Pages;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
            
        }

        private void SearchPageClick(object sender, RoutedEventArgs e)
        {
            
            SearchPage searchPage = new SearchPage();
            searchPage.line.Fill = new SolidColorBrush(Colors.White);
            _mesPageBtn.IsEnabled = true;
            _workFrame.Content = searchPage;
            _searchPageBtn.IsEnabled = false;
          
        }

        private void MessPageClick(object sender, RoutedEventArgs e)
        {
            _searchPageBtn.IsEnabled = true;
            _workFrame.Content = new MessageToAllPage();
            _mesPageBtn.IsEnabled = false;
        }

        private void BackClick(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            WindowControler.SetPosition(this, mainWindow);
            mainWindow.Show();
            this.Close();
        }
    }
}
