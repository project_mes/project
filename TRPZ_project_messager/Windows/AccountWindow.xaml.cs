﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPZ_project_messager.Model;
using TRPZ_project_messager.Pages;
using TRPZ_project_messager.Windows;

namespace TRPZ_project_messager
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class AccountWindow : Window
    {
        public AccountWindow(User user)
        {
            InitializeComponent();
            _menu.Content = new MenuPage();
            _accountEmail.Text = user.UserEmail;
            _accountLogin.Text = user.UserLogin;
            _accountNumber.Text = user.UserPhoneNumber;
            BitmapImage image = new BitmapImage(new Uri(user.UserImg, UriKind.Absolute));
            _accountImg.Source = image;

        }
       
        private void ChangeAccontInfoClick(object sender, RoutedEventArgs e)
        {
            ChangeAccInfoWindow changeAccInfoWindow = new ChangeAccInfoWindow(Account.myUser.UserId);
            changeAccInfoWindow.Owner = this;
            changeAccInfoWindow.Show();

        }
    }
}
