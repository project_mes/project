﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.Pages;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для IpAdminCheckWindow.xaml
    /// </summary>
    public partial class IpAdminCheckWindow : Window
    {
        public IpAdminCheckWindow()
        {
            InitializeComponent();
            _deviceFrame.Content = new IpConfigPage(Account.myUser);
        }

        private void BackClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
