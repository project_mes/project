﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TRPZ_project_messager.UC;

namespace TRPZ_project_messager.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddToGroupChatWindow.xaml
    /// </summary>
    public partial class AddToGroupChatWindow : Window
    {
        public AddToGroupChatWindow()
        {
            InitializeComponent();
            _groupChats.Children.Add(new UserViewInListUC(1, "http://www.youloveit.ru/uploads/gallery/main/777/youloveit_ru_svinka_pepa_peppa_pig22.png", "group chat1","10 users",600));
            _groupChats.Children.Add(new UserViewInListUC(1, "https://i.pinimg.com/originals/76/6c/f7/766cf770ea8dd3529bd8e0c41d6784be.jpg", "group chat 4","7 users", 600));
        }

        private void BackButton(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
