﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPZ_project_messager.Model
{
    public class Message
    {
        public int MessageId { get; set; }
        public DateTime MessageTime { get; set; }
        public string MessageText { get; set; }
        public User MessageUser { get; set; }
    }
}
