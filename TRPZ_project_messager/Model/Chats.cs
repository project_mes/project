﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPZ_project_messager.Model
{
    public class Chats
    {
        public int ChatId { get; set; }
        public int ChatType { get; set; } //1-solo 2-group
        public string ChatName { get; set; }
        public string ChatImg { get; set; }
        public string ChatStatus { get; set; }
      
        public User ChatAdmin { get; set; }
    }
}
