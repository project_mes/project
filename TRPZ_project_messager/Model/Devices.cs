﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPZ_project_messager.Model
{
    public class Devices
    {
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceIp { get; set; }
        public DateTime DeviceTime { get; set; }

      
    }
    
}
