﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPZ_project_messager.Model
{
    class NewPasswordModel
    {
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
    }
}
