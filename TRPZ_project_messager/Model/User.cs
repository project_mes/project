﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPZ_project_messager.Model
{
    public class User
    {
        public int UserId { get; set; }
        public string UserImg { get; set; }
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
        public string UserEmail { get; set; }
        public string UserPhoneNumber { get; set; }
        public string UserStatus { get; set; }
        public string UserRole { get; set; }
        public List<Devices> UserListDevice { get; set; }
    }
}
