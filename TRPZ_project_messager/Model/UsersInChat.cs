﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPZ_project_messager.Model
{
    class UsersInChat
    {
        public int UserInChatId { get; set; }
        public int UserId { get; set; }
        public int ChatId { get; set; }
    }
}
