﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPZ_project_messager.Model
{
    class MessageInChat
    {
        public int MessageId { get; set; }
        public int ChatId { get; set; }
    }
}
